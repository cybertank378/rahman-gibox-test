package com.gibox.rahman.view.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import androidx.paging.PagingData
import com.gibox.rahman.core.data.auth.source.remote.response.DataItem
import com.gibox.rahman.core.data.auth.source.remote.response.ResponseListUser
import com.gibox.rahman.core.data.vo.Resource
import com.gibox.rahman.core.domain.auth.usecase.AuthUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Created              : Rahman on 13/07/2022.
 * Date Created         : 13/07/2022 / 22:03.
 * ===================================================
 * Package              : com.gibox.rahman.view.ui.viewmodel
 * Project Name         : rahman.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
class UserListViewModel(private val authUseCase: AuthUseCase): ViewModel() {

    private var currentPage = 1
    var isLastPage = false

    private val _isLoadingUser =  MutableLiveData<Resource<Boolean>>()
    private val _listUsers = MutableLiveData<Resource<PagingData<DataItem>>>()
    val isLoadingUser = _isLoadingUser
    val isListUser = _listUsers

    fun getUserList() {
        viewModelScope.launch {
            _isLoadingUser.postValue(Resource.Loading(null))
           try {
               authUseCase.getUserList().collect{
                   _listUsers.postValue(Resource.Success(it))
               }
           } catch (ex : Exception){

           }
        }
    }
}