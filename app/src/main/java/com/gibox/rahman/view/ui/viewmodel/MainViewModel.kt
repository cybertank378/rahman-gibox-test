/*
 * Created by Muhamad Syafii
 * , 5/4/2022
 * Copyright (c) 2022 by Gibox Digital Asia.
 * All Rights Reserve
 */

package com.gibox.rahman.view.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.map
import com.gibox.rahman.core.data.auth.source.remote.request.LoginRequest
import com.gibox.rahman.core.data.auth.source.remote.response.DataItem
import com.gibox.rahman.core.data.auth.source.remote.response.ResponseListUser
import com.gibox.rahman.core.data.vo.Resource
import com.gibox.rahman.core.domain.auth.model.LoginEntityDomain
import com.gibox.rahman.core.domain.auth.usecase.AuthUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.Exception

class MainViewModel(private val authUseCase:AuthUseCase):ViewModel() {

    private val _isErrorRequestLogin = MutableLiveData<Resource<String>>()
    private val _dataRequestLogin = MutableLiveData<Resource<LoginEntityDomain>>()
    private val _isLoadingRequestLogin = MutableLiveData<Resource<Boolean>>()

    val isErrorRequestLogin = _isErrorRequestLogin
    val dataRequestLogin = _dataRequestLogin
    val isLoadingRequestLogin = _isLoadingRequestLogin


    fun requestLogin(loginRequest:LoginRequest){
        viewModelScope.launch {
            _isLoadingRequestLogin.postValue(Resource.Loading(null))
            try {
                authUseCase.doLogin(loginRequest).collect{
                    if (it.data != null){
                        _dataRequestLogin.postValue(Resource.Success(it.data))
                    } else {
                        _dataRequestLogin.postValue(Resource.Error("Failed get data respond", null))
                    }

                }
            } catch (ex : Exception){
                _isErrorRequestLogin.postValue(Resource.Error("${ex.message}", null))
            }


        }

    }



}





