/*
 * Created by Muhamad Syafii
 * Monday, 04/04/2022
 * Copyright (c) 2022 by Gibox Digital Asia.
 * All Rights Reserve
 */

package com.gibox.rahman.view.ui.listuser

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.gibox.rahman.core.data.vo.Resource
import com.gibox.rahman.databinding.ActivityListUserBinding
import com.gibox.rahman.util.showToast
import com.gibox.rahman.view.adapter.LoaderStateAdapter
import com.gibox.rahman.view.adapter.UsersAdapters
import com.gibox.rahman.view.ui.viewmodel.UserListViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ListUserActivity : AppCompatActivity() {


    private val binding by lazy { ActivityListUserBinding.inflate(layoutInflater) }

    private val viewModel: UserListViewModel by viewModel()

    private lateinit var usersAdapters: UsersAdapters
    private lateinit var loaderStateAdapter: LoaderStateAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupRecycleView()
        setupViewModel()
        setupObserver()


    }

    private fun setupObserver() {
        viewModel.isListUser.observe(this) { pagedData ->
            when (pagedData) {
                is Resource.Loading -> {
                    displayLoading()
                }
                is Resource.Success -> {
                    hideLoading()

                    pagedData.data?.let {
                        usersAdapters.submitData(this.lifecycle, it)
                    }
                }
                is Resource.Error -> {
                    hideLoading()
                    displayError(pagedData.message.toString())
                }
            }
        }
    }

    private fun setupRecycleView() {
        usersAdapters = UsersAdapters()
        loaderStateAdapter = LoaderStateAdapter{ usersAdapters.retry() }
        binding.run {
            userRv.setHasFixedSize(true)
            userRv.layoutManager = LinearLayoutManager(this@ListUserActivity)
            userRv.adapter = usersAdapters.withLoadStateFooter(loaderStateAdapter)
        }

    }

    private fun setupViewModel() {
       viewModel.getUserList()

    }

    private fun displayLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.progressBar.visibility = View.GONE
    }

    private fun displayError(errorMessage: String) {
        hideLoading()
        showToast(errorMessage)
    }


}