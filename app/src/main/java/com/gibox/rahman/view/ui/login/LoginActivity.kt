/*
 * Created by Muhamad Syafii
 * Monday, 04/04/2022
 * Copyright (c) 2022 by Gibox Digital Asia.
 * All Rights Reserve
 */

package com.gibox.rahman.view.ui.login

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.gibox.rahman.core.data.auth.source.remote.request.LoginRequest
import com.gibox.rahman.core.data.vo.Resource
import com.gibox.rahman.databinding.ActivityLoginBinding
import com.gibox.rahman.util.isValidEmail
import com.gibox.rahman.util.openActivity
import com.gibox.rahman.util.showToast
import com.gibox.rahman.view.ui.listuser.ListUserActivity
import com.gibox.rahman.view.ui.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {


    private val binding by lazy { ActivityLoginBinding.inflate(layoutInflater) }
    private val viewModel: MainViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setObserver()



        initClickListener()

    }

    private fun setObserver() {
        viewModel.dataRequestLogin.observe(this) {loginRespond ->
            when (loginRespond) {
                is Resource.Loading -> {
                    displayLoading()
                }
                is Resource.Success -> {
                    hideLoading()
                    openActivity(ListUserActivity::class.java)
                }
                is Resource.Error -> {
                    hideLoading()
                    displayError(loginRespond.message.toString())
                }
            }

        }
    }

    private fun initClickListener() {
        binding.loginBtn.setOnClickListener {
            if (isValidate()) {
                viewModel.requestLogin(LoginRequest(binding.emailTv.text.toString(),
                    binding.passTv.toString()))
            }

        }


    }

    private fun isValidate(): Boolean = validateEmail() && validatePassword()


    /**
     * 1) field must not be empty
     * 2) text should matches email address format
     */
    private fun validateEmail(): Boolean {
        if (binding.emailTv.text.toString().trim().isEmpty()) {
            binding.emailInputLayout.error = "Required Field!"
            binding.emailTv.requestFocus()
            return false
        } else if (!isValidEmail(binding.emailTv.text.toString())) {
            binding.emailInputLayout.error = "Invalid Email!"
            binding.emailTv.requestFocus()
            return false
        } else {
            binding.emailInputLayout.isErrorEnabled = false
        }
        return true
    }


    private fun validatePassword(): Boolean {
        if (binding.passTv.text.toString().trim().isEmpty()) {
            binding.passwordInputLayout.error = "Required Field!"
            binding.passTv.requestFocus()
            return false
        } else if (binding.passTv.text.toString().length < 6) {
            binding.passwordInputLayout.error = "password can't be less than 6"
            binding.passTv.requestFocus()
            return false
        } else {
            binding.passwordInputLayout.isErrorEnabled = false
        }
        return true
    }


    private fun displayLoading() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.progressBar.visibility = View.GONE
    }

    private fun displayError(errorMessage: String) {
        hideLoading()
        showToast(errorMessage)
    }


}