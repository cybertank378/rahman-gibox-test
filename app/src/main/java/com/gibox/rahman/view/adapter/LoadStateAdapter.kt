package com.gibox.rahman.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gibox.rahman.databinding.PagingFooterBinding

/**
 * Created              : Rahman on 14/07/2022.
 * Date Created         : 14/07/2022 / 00:27.
 * ===================================================
 * Package              : com.gibox.rahman.view.adapter
 * Project Name         : rahman.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
class LoaderStateAdapter(private val retry: () -> Unit) : LoadStateAdapter<LoaderStateAdapter.LoaderViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoaderViewHolder {
        val itemBinding = PagingFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoaderViewHolder(itemBinding, retry)
    }


    override fun onBindViewHolder(holder: LoaderViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }



    class LoaderViewHolder(private val binding: PagingFooterBinding, private val retry: () -> Unit) : RecyclerView.ViewHolder(binding.root) {

        fun bind(loadState: LoadState) {
            if (loadState is LoadState.Loading) {
                binding.run {
                    progressBar.visibility = View.VISIBLE
                    errorMsg.visibility = View.GONE
                    retryButton.visibility = View.GONE
                }
            } else {
                binding.run {
                    progressBar.visibility = View.GONE
                    errorMsg.visibility = View.VISIBLE
                    retryButton.visibility = View.VISIBLE
                    retryButton.setOnClickListener{
                        retry()
                    }
                }
            }
        }


    }


}