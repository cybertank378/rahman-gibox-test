package com.gibox.rahman.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.gibox.rahman.core.data.auth.source.remote.response.DataItem
import com.gibox.rahman.databinding.ItemUserBinding
import com.gibox.rahman.util.loadImage

/**
 * Created              : Rahman on 13/07/2022.
 * Date Created         : 13/07/2022 / 15:43.
 * ===================================================
 * Package              : com.gibox.rahman.view.adapter
 * Project Name         : rahman.
 * Copyright            : Copyright @ 2022 cybertank378.
 */
class UsersAdapters : PagingDataAdapter<DataItem, RecyclerView.ViewHolder>(DiffUtilCallBack()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemBinding = ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? UsersViewHolder)?.bind(getItem(position)!!)
    }
    inner class UsersViewHolder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(dataItem: DataItem) {
            binding.run {
                dataItem.avatar?.let {
                    avatarImg.loadImage(it)
                }
                emailUserTv.text = dataItem.email
                usernameTv.text = String.format("%s %s", dataItem.firstName, dataItem.lastName)

            }
        }
    }
}

class DiffUtilCallBack : DiffUtil.ItemCallback<DataItem>() {
    override fun areItemsTheSame(oldItem: DataItem, newItem: DataItem) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: DataItem, newItem: DataItem): Boolean {
        return oldItem.id == newItem.id
                && oldItem.email == newItem.email
                && oldItem.firstName == newItem.firstName
    }
}
