/*
 * Created by Muhamad Syafii
 * Monday, 04/04/2022
 * Copyright (c) 2022 by Gibox Digital Asia.
 * All Rights Reserve
 */

package com.gibox.rahman.di

import com.gibox.rahman.view.ui.viewmodel.MainViewModel
import com.gibox.rahman.view.ui.viewmodel.UserListViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
    viewModel { UserListViewModel(get()) }
}