/*
 * Created by Muhamad Syafii
 * Friday, 21/1/2022
 * Copyright (c) 2022 by Gibox Digital Asia.
 * All Rights Reserve
 */

package com.gibox.rahman.util

interface ItemClickListener<T> {
    fun onClick(data : T)
}